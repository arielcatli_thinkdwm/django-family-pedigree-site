from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.urls import reverse

from . forms import CreateUserForm, EditUserProfileForm


class Login(LoginView):
    template_name = "account/login.html"


class Logout(LogoutView):
    template_name = "account/logout.html"


@login_required
def success(request):
    return render(request, 'account/success.html', {})


def register(request):
    if request.method == "GET":
        form = CreateUserForm()
        return render(request, "account/register.html", {"form": form})
    elif request.method == "POST":
        form = CreateUserForm(request.POST)

        if not form.is_valid():
            errors = [error for field_error in form.errors for error in form.errors[field_error]]
            for error in errors:
                messages.error(request, error)

            return render(request,
                          'account/register.html',
                          {"form": form,
                           "error_message": errors})

        form.save()
        messages.success(request, "Account successfully created. Please, log in with the new account.")
        return HttpResponseRedirect(reverse('account:login'))


@login_required
def edit(request):
    if request.method == "GET":
        form = EditUserProfileForm(instance=request.user)
        return render(request, 'account/edit.html', {"form": form})
    elif request.method == "POST":
        form = EditUserProfileForm(request.POST, instance=request.user)

        if not form.is_valid():
            errors = [error for field_error in form.errors for error in form.errors[field_error]]
            for error in errors:
                messages.error(request, error)

            return render(request,
                          'account/edit.html',
                          {"form": form,
                           "error_message": errors})

        form.save()
        return HttpResponseRedirect(reverse('account:success'))



