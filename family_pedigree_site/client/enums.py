from django.db.models import IntegerChoices, TextChoices
from django.utils.translation import gettext_lazy as _


class VitalStatus(TextChoices):
    DECEASED = 'DC', _('Deceased')
    LIVING = 'LI', _('Living')


class RelationshipLevel(IntegerChoices):
    UNSPECIFIED = 0

    GRANDPARENTHOOD = 1
    SPOUSEHOOD = 2
    PARENTHOOD = 3
    SIBLINGHOOD = 4
    UNCLE_AUNTIE_NIECE_NEPHEW = 5
    COUSIN = 6

    IN_LAW_GRANDPARENTHOOD = 8
    IN_LAW_SPOUSEHOOD = 9
    IN_LAW_PARENTHOOD = 10
    IN_LAW_SIBLINGHOOD = 11
    IN_LAW_UNCLE_AUNTIE_NIECE_NEPHEW = 12
    IN_LAW_COUSIN = 13


class Sex(TextChoices):
    UNSPECIFIED = 'X', _("Unspecified")
    MALE = 'M', _("Male")
    FEMALE = 'F', _("Female")


class FamilySide(TextChoices):
    UNSPECIFIED = 'X', _("Unspecified")
    FATHER = 'M', _("Father")
    MOTHER = 'F', _("Mother")


class MaleRole(IntegerChoices):
    UNSPECIFIED = 0, _('Unspecified')

    GRANDFATHER = 1, _('Grandfather')
    HUSBAND = 2, _('Husband')
    FATHER = 3, _('Father')
    BROTHER = 4, _('Brother')
    UNCLE = 5, _('Uncle')
    NEPHEW = 6, _('Nephew')
    COUSIN = 7, _('Cousin')

    IN_LAW_GRANDFATHER = 8, _('Grandfather-in-law')
    IN_LAW_HUSBAND = 9, _('Husband-in-law')
    IN_LAW_FATHER = 10, _('Father-in-law')
    IN_LAW_BROTHER = 11, _('Brother-in-law')
    IN_LAW_UNCLE = 12, _('Uncle-in-law')
    IN_LAW_NEPHEW = 13, _('Nephew-in-law')
    IN_LAW_COUSIN = 14, _('Cousin-in-law')


class FemaleRole(IntegerChoices):
    UNSPECIFIED = 0, _('Unspecified')

    GRANDMOTHER = 1, _('Grandmother')
    WIFE = 2, _('Wife')
    MOTHER = 3, _('Mother')
    SISTER = 4, _('Sister')
    AUNTIE = 5, _('Auntie')
    NIECE = 6, _('Niece')
    COUSIN = 7, _('Cousin')

    IN_LAW_GRANDMOTHER = 8, _('Grandmother-in-law')
    IN_LAW_WIFE = 9, _('Wife-in-law')
    IN_LAW_MOTHER = 10, _('Mother-in-law')
    IN_LAW_SISTER = 11, _('Sister-in-law')
    IN_LAW_AUNTIE = 12, _('Auntie-in-law')
    IN_LAW_NIECE = 13, _('Niece-in-law')
    IN_LAW_COUSIN = 14, _('Cousin-in-law')




