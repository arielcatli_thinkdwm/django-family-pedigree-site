from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from rest_framework import status
import requests
import json
import os

from . import forms

AUTH_API_ROOT_URL = os.getenv("API_ROOT_URL")
AUTH_API_USER = AUTH_API_ROOT_URL + "user/"
AUTH_API_USER_INFO_URL = AUTH_API_ROOT_URL + "token/userinfo/"
AUTH_API_TOKEN_URL = os.getenv("AUTH_API_TOKEN_URL")
AUTH_API_AUTH_REQUEST_URL = os.getenv("AUTH_API_AUTH_REQUEST_URL")
AUTH_API_CLIENT_ID = os.getenv("CLIENT_ID")
AUTH_API_CLIENT_SECRET = os.getenv("CLIENT_SECRET")

API_ENDPOINTS = {
    "root": AUTH_API_ROOT_URL,
    "user_detail": AUTH_API_USER,
    "create_member": "http://127.0.0.1:8001/family/members/",

}


def index(request):
    return render(request, "client/index.html", {})


def app_index(request):
    if request.session.get('access-token', None) is None:
        return initiate_oauth2_authorization()

    user_info = get_user_info(request)
    data = {
        "userinfo": user_info
    }
    return render(request, "client/app_index.html", {"data": data})


def get_user_info(request):
    user_info = send_request_with_token(request=request,
                                        method="GET",
                                        url=AUTH_API_USER_INFO_URL)
    user = json.loads(user_info)
    return user


def send_request_with_token(request, method, url, params=None, payload=None):
    access_token = request.session.get('access-token', "")
    headers = {
        "Authorization": "Bearer " + access_token
    }
    if method == "GET":
        message = requests.get(url, params=params, headers=headers)
    elif method == "POST":
        message = requests.post(url, params=params, headers=headers)

    return message.text


def member_detail(request, pk):
    headers = {
        "Authorization": "Bearer " + "j0EpBTki6dFosKTjtPbq6QiKmK2A8V"
    }
    member = requests.get("http://127.0.0.1:8000/api/v1/member/1", headers=headers)
    return render(request, "client/member.html", {"member": member.text})




#################################################################################################
#                                  OAuth2 related methods                                       #
#################################################################################################


def initiate_oauth2_authorization():
    """
    Assembles the OAuth2 Authorization request to be sent and redirect to the assembled URL.
    """
    credentials = {
        "response_type": "code",
        "client_id": AUTH_API_CLIENT_ID,
        "redirect_uri": "http://127.0.0.1:8001/authentication/"
    }
    data = send_authentication_request_to_oauth2(credentials)
    return redirect(data.url)


def send_authentication_request_to_oauth2(credentials):
    """
    Create the GET request for Authorization request.
    """
    request = requests.get(AUTH_API_AUTH_REQUEST_URL,
                           params=credentials)
    return request


def auth_handler_callback(request):
    """
    View for handling redirect_uri.
    Responsible for requesting token from the authorization code.
    """
    if request.method == "GET":
        authorization_code = request.GET['code']
        credentials = {
            "grant_type": "authorization_code",
            "code": authorization_code,
            "client_id": AUTH_API_CLIENT_ID,
            "client_secret": AUTH_API_CLIENT_SECRET,
            "redirect_uri": "http://127.0.0.1:8001/authentication/",
        }
        token = get_access_token(credentials)
        token = json.loads(token)["access_token"]
        request.session['access-token'] = token

        return redirect(reverse('client:app_index'))


def get_access_token(credentials):
    """
    Creates POST request for requesting access token.
    """
    request = requests.post(AUTH_API_TOKEN_URL, credentials)
    auth_response = request.text
    return auth_response


#################################################################################################
#                               End of OAuth2 related methods                                   #
#################################################################################################


def access_member_data(request):
    token = request.session["access-token"]
    headers = {"Authorization": "Bearer " + token}
    request_from_api = requests.get("http://127.0.0.1:8000/api/v1/member/1/genealogy", headers=headers)
    return render(request, 'client/index.html', {"data": request_from_api.text})

# ######################################################


def has_access_token(request):
    return True if request.session.get('access-token', None) is not None else False


def member_list(request):
    if request.method == "GET":
        form = forms.MemberCreationForm()
        return render(request, "client/member-create.html", {"form": form})
    elif request.method == "POST":
        form = forms.MemberCreationForm(request.POST)
        if not form.is_valid():
            messages.error(form.errors)
            return HttpResponse("something wrong")

        # create_request = requests.posts(API_ENDPOINTS.create_member,
        #                                 data=form.cleaned_data)
        print(request.POST['csrfmiddlewaretoken'])
        return JsonResponse(request.POST['csrfmiddlewaretoken'], safe=False)






