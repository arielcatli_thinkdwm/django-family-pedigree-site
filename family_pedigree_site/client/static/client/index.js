class Member
{
    constructor(first_name,
                last_name,
                birthday,
                sex,
                vital_status) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.sex = sex;
        this.vital_status = vital_status;
    }

    get name()
    {
        return `${this.first_name} ${this.last_name}`;
    }

//    get birthday()
//    {
//        return this.birthday;
//    }

//    get sex()
//    {
//        return this.sex;
//    }

    get isAlive()
    {
        return this.vital_status == "LI" ? true : false;
    }
}

m = new Member("Ariel",
               "Catli",
               "07/14/1995",
               "M",
               "LI");

console.log(m.name);
console.log(m.birthday);
console.log(m.isAlive);

fetch("http://127.0.0.1:8001/family/member/1")
.then((resp) => resp.json())
.then((data) => console.log(data))
.catch((e) => console.log(`ERROR IS THIS: ${e}`));
