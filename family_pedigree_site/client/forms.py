from django import forms

from . import enums


class MemberCreationForm(forms.Form):
    first_name = forms.CharField(max_length=200)
    last_name = forms.CharField(max_length=200)
    birthday = forms.DateField()
    sex = forms.ChoiceField(choices=enums.Sex.choices,
                            initial=enums.Sex.UNSPECIFIED)
    vital_status = forms.ChoiceField(choices=enums.VitalStatus.choices,
                                     initial=enums.VitalStatus.LIVING)

