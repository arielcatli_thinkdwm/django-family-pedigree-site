from django.urls import path

from . import views

app_name = "client"
urlpatterns = [
    path('', views.index, name='index'),
    path('family/', views.app_index, name='app_index'),
    path('family/<int:pk>/', views.member_detail, name="member"),
    path('family/members/', views.member_list, name="member_list"),
    path('authentication/', views.auth_handler_callback, name="authentication"),
]

"""
[DONE] / - index, point to the main app
family/ - show the genealogy of the current user
family/member/ - [GET] get the list of the members
family/member/add - [GET] get the add member form
family/member/add - [POST] add the member
family/member/<int:pk>/ - [GET] show the info of the member, including the link to edit or delete
family/member/<int:pk>/edit - [GET] show the edit form for the member
family/member/<int:pk>/edit - [POST] update the member
family/member/<int:pk>/delete - [POST] delete the member
family/relationship/add/ - [GET] show the add relationship (parent) form
family/relationship/add/ - [POST] add the relationship (parent) data
family/relationship/<int:pk> - [GET] parent relationship information
family/relationship/<int:pk>/edit - [GET] get parent relationship edit form
family/relationship/<int:pk>/edit - [POST] submit relationship edit form
family/relationship/<int:pk>/delete - [POST] delete parent relationship
family/relationship/spouse/add - [GET] show the add relationship (spouse) form
family/relationship/spouse/add - [POST] add relationship (spouse)
family/relationship/spouse/<int:pk>/ - [GET] spousehood relationship information
family/relationship/spouse/<int:pk>/edit - [GET] spousehood relationship edit form
family/relationship/spouse/<int:pk>/edit - [POST] submit spousehood relationship form
family/relationship/spouse/<int:pk>/delete - [POST] delete spousehoold relationship






"""
